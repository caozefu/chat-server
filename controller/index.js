const getUserInfo = require('./modules/User/getUserInfo');
const updateUserInfo = require('./modules/User/updateUserInfo');
const loginStatus = require('./modules/User/loginStatus');

module.exports = {
    updateUserInfo,
    getUserInfo,
    loginStatus
};
