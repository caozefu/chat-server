const resJson = require('../../../utils/resJson');
const tokenUtils = require('../../../utils/token');
const getUserInfoModel = require('../../../model/getUserInfo');
const updateUserInfo = require('../../../model/Friend/addFriendRequest');
const utils = require('../../../utils/index');

module.exports = async (req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
    let token = utils.getCookie(req.headers.cookie, 'token');
    if (!token || token === 'undefined') {
        res.end(JSON.stringify(resJson.returnError(500, '用户未登录')));
        return;
    }
    const {key, value} = req.body;

    const data = tokenUtils.verifyToken(token);
    await updateUserInfo(data.data.user_uid, key, value).then(data => {
        res.end(resJson.returnSuccess('修改成功'))
    }).catch(e => {
        res.end(resJson.returnError(500, e));
    })

};
