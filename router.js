const express = require('express');
const router = express.Router();
const User = require('./controller/modules/User');
const controller = require('./controller');

/*
* User
* 用户相关模块
* */
// 登陆状态
router.get('/login/status', controller.loginStatus);
// 获取用户信息
router.get('/getUserInfo', controller.getUserInfo);
// 修改用户信息
router.post('/updateUserInfo', controller.updateUserInfo);
// 注册
router.post('/register', User.register);
// 登陆
router.post('/login', User.login);
// 获取好友列表
router.get('/getFriends', User.getFriends);
// 搜索好友
router.get('/searchFriends', User.searchFriends);
// 根据id搜索好友
router.get('/searchFriendById', User.searchFriendById);
// 添加好友
router.post('/addFriend', User.addFriend);
// 同意添加好友申请
router.post('/applyFriend', User.applyFriend);
// 获取好友申请列表
router.get('/getRequest', User.getRequest);


module.exports = router;
