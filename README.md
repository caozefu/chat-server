# chat-server

聊天软件服务端  

Node + Express + Socket.io 实现以下功能:  

* 注册/登陆
* 修改用户信息
* 搜索/添加好友
* 好友间私聊，保存聊天信息

---
``npm install``

``npm run start``
