function startSocket(io) {
    io.on('connection', function (socket) {
        socket.on('chat message', function (msg) {
            io.emit(msg.id, msg);
            io.emit(msg.target, msg);
        });
    });
}


module.exports = startSocket;
