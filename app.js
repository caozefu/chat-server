const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const fs = require('fs');
const multer = require('multer');
const ossConfig = require('./config/ossConfig');
const MAO = require('multer-aliyun-oss');
const OSS = require('./libs/oss');
require('./socket/index')(io);

const router = require('./router');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api', router);

const upload = multer({
    storage: MAO({
        config: ossConfig
    })
});

app.post('/api/upload', upload.single('portrait'), (req, res) => {
    // let url = OSS.signatureUrl(req.file.filename);
    res.send({code: '200', url: req.file.url})
});

http.listen(port, (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log('start server at ' + port)
});
